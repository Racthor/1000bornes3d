﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Carte : MonoBehaviour, ICarte
{
    private string nom;
    private TypeCarte type;
    private Joueur possesseur;

    abstract public void EffetCarte(Joueur j);
}
