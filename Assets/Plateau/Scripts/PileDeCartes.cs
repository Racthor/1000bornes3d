﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PileDeCartes : Stack<Carte>
{
    PileDeCartes() : base(106) {}

    //Stack ou LIFO
    void ajout(Carte c)
    {
        if (this.Count < 106)
        {
            this.Push(c);
        } else
        {
            throw new Exception("[Warning][PileDeCartes::ajout(Carte c)] Le jeu ne peut contenir plus de 106 cartes.");
        }
    }

}
